#!/bin/bash

[[ ! -f "$HOME/.netrc" ]] && echo 'create ~/.netrc file for webdav auth' && exit

REMOTE_DIR="gitout"
LOCAL_DIR="static/gitout"
mkdir -p $LOCAL_DIR
wget --recursive --timestamping --quiet --show-progress \
	--no-host-directories --no-parent --cut-dirs=1  \
	--user-agent='Wget/linux (+https://2vek.com)' \
	--directory-prefix=$LOCAL_DIR --reject index.html \
	https://myfiles.fastmail.com/$REMOTE_DIR/
	# use .netrc instead of below
	# --user='' --password='' \
rm -f $LOCAL_DIR/index.html.tmp
