#!/bin/bash

[[ $# -lt 1 ]] && echo "$0 [FILES_TO_UPLOAD]" &&exit

RDIR="gitout"
UFILES="${@:1}"

for UFILE in $UFILES;do
	echo "- $UFILE";
	UFPATH="$RDIR/${UFILE##*/}"

	if [ -f "$UFPATH" ]; then
		read -p 'file exists!! OVERWRITE [y/n]: ' -n 1 -r
		[[ ! $REPLY =~ ^[Yy]$ ]] && echo && exit
	fi
done

read -p 'Upload above files [y/n]: ' -n 1 -r
[[ ! $REPLY =~ ^[Yy]$ ]] && echo && exit

NUMF=0
for UFILE in $UFILES; do
	curl --netrc --upload-file "$UFILE" \
		https://myfiles.fastmail.com/$RDIR/ \
		--progress-bar | tee /dev/null \
	&& (( NUMF++ ))
done

echo; echo "$NUMF files uploaded to $RDIR."
