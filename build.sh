#!/bin/bash

./webdav/dl.sh

mkdir -p static/{css,js}
sassc -t compact assets/css/style.scss  > static/css/style.css
cat assets/js/*.js > static/js/script.js

hugo \
	--cleanDestinationDir		\
	--destination build		\
	--quiet

REPO_NAME="$(basename $(git config --get remote.origin.url) .git)"
CURRENT_TIME="$(date +%s)"
COMMIT_HEAD="$(git rev-parse --short=8 HEAD)"
[ $# -ne 0 ] && [ "$1" == "--via-hook" ] && BUILD_SRC='hook' || BUILD_SRC='cli'

echo "$REPO_NAME,$CURRENT_TIME,$COMMIT_HEAD,$BUILD_SRC" > hook/build.csv
