<?php

class Handler {
  const REPO_NAME = '2vek/wlog';

  private $response = null;
  private $response_is = true;
  private $response_code = 200;

  private $payload = null;

  private function buildExecCmd($branch) {
    return 'cd ../../'
      . ' && git checkout ' . $branch
      . ' && git fetch'
      . ' && git reset --hard origin/' . $branch
      . ' && ./build.sh --via-hook';
  }

  function CSV($file) {
    if (file_exists($file))
      return str_getcsv(fgets(fopen($file, 'r')));
    else return array();
  }

  function JSON() {
    $json = json_decode($this->payload, true);
    if ($json !== null && json_last_error() === JSON_ERROR_NONE)
      return $json;
    else array();
  }

  function getReq($csv) {
    $build = array(
      "name" => $csv[0],
      "time" => date('H:i:s', $csv[1]),
      "date" => date('d/m/y', $csv[1]),
      "sha"  => $csv[2],
      "via"  => $csv[3],
    );

    require_once 'tmpl.php';
    $this->response_is = false;
  }

  function readPayload() {
    if ($_SERVER['HTTP_CONTENT_TYPE'] === 'application/json') {
      $this->payload = file_get_contents('php://input');
      return true;
    } else return false;
  }

  function validPayload($csv) {
    $gitlab_event = $_SERVER['HTTP_X_GITLAB_EVENT'];
    $gitlab_token = $_SERVER['HTTP_X_GITLAB_TOKEN'];
    $hook_token = $csv[0] ? $csv[0] : '';

    if (isset($gitlab_event) && ($gitlab_event === 'Push Hook') && isset($hook_token) &&
        isset($gitlab_token) && ($gitlab_token === $hook_token))
        return true;
    return false;
  }

  function processReq($request_json) {
    $ref = substr($request_json['ref'], strlen('refs/heads/'));
    if ($request_json['object_kind'] === 'push' &&
        $request_json['project']['path_with_namespace'] === self::REPO_NAME &&
        $request_json['project']['default_branch'] === $ref)
          exec($this->buildExecCmd($ref));
  }

  function postReq() {
    $this->response = 'OK.';
  }

  function invalid() {
    $this->response_code = 400;
    $this->response = 'Wat?';
  }

  function view() {
    if ($this->response_is) {
      http_response_code($this->response_code);
      echo $this->response;
    }
    $this->response_is = false;
  }
}
