<!Doctype html>
<html lang=en>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>build</title>
  <style>
html { box-sizing: border-box; }
*, *:before, *:after { box-sizing: inherit; }
.repo { margin: 1em 0; }
.repo .name { margin: 0.5em; font-size: 1.5em;
  text-transform: capitalize; text-decoration: underline; }
.repo .info { margin-left: 1.5em; width: 200px; }
.repo .info .data {font-family: monospace; font-size:1.5em; }
  </style>
</head>
<body>
  <div class="repo">
    <?php if (empty($build)): ?>
      <div class="info">No builds yet!</div>
    <?php else: ?>
      <div class="name"><?= $build['name'] ?></div>
      <table class="info">
        <tr>
          <td>Build time</td>
          <td class="data"><?= $build["time"] ?></td>
        </tr>
        <tr>
          <td>Build date</td>
          <td class="data"><?= $build["date"] ?></td>
        </tr>
        <tr>
          <td>Commit</td>
          <td class="data"><?= $build["sha"] ?></td>
        </tr>
        <tr>
          <td>Build via</td>
          <td class="data"><?= $build["via"] ?></td>
        </tr>
      </table>
    <?php endif;?>
  </div>
</body>
</html>
