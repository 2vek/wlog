<?php

require_once '../src/handler.php';

$hook = new Handler();
switch ($_SERVER['REQUEST_METHOD']) {
  case 'GET':
    $hook->getReq($hook->CSV('../build.csv'));
  break;
  case 'POST':
    $token = $hook->CSV('../token.csv');
    if ($hook->readPayload() && $hook->validPayload($token)) {
      $hook->postReq();
      if (function_exists('fastcgi_finish_request')) {
        $hook->view();
        fastcgi_finish_request();
      }
      $hook->processReq($hook->JSON());
    } else {
      $hook->invalid();
    }
  break;
  default:
    $hook->invalid();
}
$hook->view();
