#!/bin/bash

hash hugo 2>/dev/null || { echo "install hugo." && exit; }
hash sassc 2>/dev/null || { echo "install sassc." && exit; }
hash inotifywait 2>/dev/null || { echo "install inotifywait." && exit; }

kill_on_exit() {
	local pids=$(jobs -pr);
	[ -n "$pids" ] && kill -15 $pids;
};
trap 'kill_on_exit' INT TERM EXIT

export DEBUG=1
hugo server 			\
	--bind 0.0.0.0		\
	--buildDrafts		\
	--buildExpired		\
	--buildFuture		\
	--cleanDestinationDir	\
	--disableFastRender	\
	--navigateToChanged	\
	--noHTTPCache		\
	--port 1313		&

ASST="assets"
mkdir -p static/{css,js}
touch static/{css/style.css,js/script.js}
while read file; do
	case "${file##*.}" in
	"scss") printf '> SASS: %s' "$file"
		sassc -t compact assets/css/style.scss > static/css/style.css
		;;
	"js")	printf '> JS: %s' "$file"
		cat assets/js/*.js > static/js/script.js
		;;
	esac
done < <(inotifywait --monitor --recursive --exclude '.*~$' \
		--event close_write --format "%w%f" assets)
