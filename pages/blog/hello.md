+++
title = "Hello, World"
date = "2018-03-06"
tags = [
    "meta"
]
+++

Hello, I am Vivek Verma. I live in Delhi/NCR. I make web apps for living.
In past decade of programming, I have few things to share about things that I
have learned.

At this blog, I intend to post every other day about things that interest me,
mostly related to programming. [Subscribe to blog feed][feed], to not miss out
on those new blog posts. In the meantime, you can look  into source code of
[this site on github][wlog].

See you around.

[feed]: https://2vek.com/feed.xml
[wlog]: https://github.com/2vek/wlog

